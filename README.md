# user-cookbook

A basic cookbook that creates a user and, optionally, a file in their home directory.

For more details please see the article at <https://jvt.me/posts/2017/05/25/chef-gitlab-ci-kitchen-docker/>.
