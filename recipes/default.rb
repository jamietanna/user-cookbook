#
# Cookbook:: user-cookbook
# Recipe:: default
#
# Copyright:: 2017, Jamie Tanna
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

group 'create the group' do
  group_name node['group']
  not_if { node['group'].nil? }
end

user "create user #{node['user']}" do
  username node['user']
  group node['group']
  manage_home true
end

file 'creates the hello.txt file' do
  path "/home/#{node['user']}/hello.txt"
  content "hello #{node['user']}"
  mode '0600'
  owner node['user']
  only_if { node['hello'] }
end
