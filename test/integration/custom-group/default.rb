describe user('jamie') do
  it { should exist }
  its('groups') { should eq ['test'] }
end

describe group('test') do
  it { should exist }
end

describe directory('/home/jamie') do
  it { should exist }
end

