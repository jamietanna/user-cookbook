describe user('everybody') do
  it { should exist }
  its('groups') { should eq ['everybody'] }
end

describe group('everybody') do
  it { should exist }
end

describe directory('/home/everybody') do
  it { should exist }
end

describe file('/home/everybody/hello.txt') do
  it { should exist }
  its('mode') { should cmp '0600' }
  its('owner') { should eq 'everybody' }
  its('content') { should eq 'hello everybody' }
end
