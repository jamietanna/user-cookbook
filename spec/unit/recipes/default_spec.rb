#
# Cookbook:: user-cookbook
# Spec:: default
#
# Copyright:: 2017, Jamie Tanna
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require 'spec_helper'

describe 'user-cookbook::default' do
  context 'When all attributes are default, on an Ubuntu 16.04' do
    let(:chef_run) do
      # for a complete list of available platforms and versions see:
      # https://github.com/customink/fauxhai/blob/master/PLATFORMS.md
      runner = ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '16.04')
      runner.converge(described_recipe)
    end

    it 'converges successfully' do
      expect { chef_run }.to_not raise_error
    end

    it 'doesn\'t create the users group' do
      expect(chef_run).to_not create_group('create the group')
    end

    it 'creates the jamie user' do
      expect(chef_run).to create_user('create user jamie')
        .with(username: 'jamie')
        .with(group: nil)
        .with(manage_home: true)
    end

    it 'doesn\'t create the hello.txt file' do
      expect(chef_run).to_not create_file('creates the hello.txt file')
    end
  end

  context 'When the user attribute is set' do
    let(:chef_run) do
      runner = ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '16.04') do |node|
        node.automatic['user'] = 'test'
      end
      runner.converge(described_recipe)
    end

    it 'converges successfully' do
      expect { chef_run }.to_not raise_error
    end

    it 'doesn\'t create the users group' do
      expect(chef_run).to_not create_group('create the group')
    end

    it 'creates the test user' do
      expect(chef_run).to create_user('create user test')
        .with(username: 'test')
        .with(group: nil)
        .with(manage_home: true)
    end

    it 'doesn\'t create the hello.txt file' do
      expect(chef_run).to_not create_file('creates the hello.txt file')
    end
  end

  context 'When the user and group attributes are set' do
    let(:chef_run) do
      runner = ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '16.04') do |node|
        node.automatic['user'] = 'test'
        node.automatic['group'] = 'users'
      end
      runner.converge(described_recipe)
    end

    it 'converges successfully' do
      expect { chef_run }.to_not raise_error
    end

    it 'creates the users group' do
      expect(chef_run).to create_group('create the group')
        .with(group_name: 'users')
    end

    it 'creates the test user' do
      expect(chef_run).to create_user('create user test')
        .with(username: 'test')
        .with(group: 'users')
        .with(manage_home: true)
    end

    it 'doesn\'t create the hello.txt file' do
      expect(chef_run).to_not create_file('creates the hello.txt file')
    end
  end

  context 'When the hello attribute is set' do
    let(:chef_run) do
      runner = ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '16.04') do |node|
        node.automatic['user'] = 'jamie'
        node.automatic['hello'] = true
      end
      runner.converge(described_recipe)
    end

    it 'converges successfully' do
      expect { chef_run }.to_not raise_error
    end

    it 'doesn\'t create the users group' do
      expect(chef_run).to_not create_group('create the group')
    end

    it 'creates the hello.txt file' do
      expect(chef_run).to create_file('creates the hello.txt file')
        .with(path: '/home/jamie/hello.txt')
        .with(content: 'hello jamie')
        .with(mode: '0600')
        .with(owner: 'jamie')
    end
  end
end
